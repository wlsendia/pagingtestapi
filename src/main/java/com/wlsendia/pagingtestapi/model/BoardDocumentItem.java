package com.wlsendia.pagingtestapi.model;

import com.wlsendia.pagingtestapi.entity.BoardDocument;
import com.wlsendia.pagingtestapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardDocumentItem {

    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "게시글 제목")
    private String title;

    @ApiModelProperty(notes = "작성자 이름")
    private String writerName;

    @ApiModelProperty(notes = "작성일")
    private LocalDateTime dateCreate;

    @ApiModelProperty(notes = "조회수")
    private Integer viewCount;


    private BoardDocumentItem(BoardDocumentItemBuilder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.writerName = builder.writerName;
        this.dateCreate = builder.dateCreate;
        this.viewCount = builder.viewCount;

    }

    public static class BoardDocumentItemBuilder implements CommonModelBuilder<BoardDocumentItem> {

        private final Long id;
        private final String title;
        private final String writerName;
        private final LocalDateTime dateCreate;
        private final Integer viewCount;

        public BoardDocumentItemBuilder(BoardDocument boardDocument) {
            this.id = boardDocument.getId();
            this.title = boardDocument.getTitle();
            this.writerName = boardDocument.getWriterName();
            this.dateCreate = boardDocument.getDateCreate();
            this.viewCount = boardDocument.getViewCount();
        }

        @Override
        public BoardDocumentItem build() {
            return new BoardDocumentItem(this);
        }
    }

}
