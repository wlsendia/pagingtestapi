package com.wlsendia.pagingtestapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
