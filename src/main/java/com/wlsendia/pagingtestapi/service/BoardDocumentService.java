package com.wlsendia.pagingtestapi.service;

import com.wlsendia.pagingtestapi.entity.BoardDocument;
import com.wlsendia.pagingtestapi.model.BoardDocumentCreateRequest;
import com.wlsendia.pagingtestapi.model.BoardDocumentItem;
import com.wlsendia.pagingtestapi.model.ListResult;
import com.wlsendia.pagingtestapi.repository.BoardCommentRepository;
import com.wlsendia.pagingtestapi.repository.BoardDocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardDocumentService {
    private final BoardDocumentRepository boardDocumentRepository;

    private final BoardCommentRepository boardCommentRepository;

    public void setDocument(BoardDocumentCreateRequest createRequest) {
        BoardDocument addData = new BoardDocument.BoardDocumentBuilder(createRequest).build();

        boardDocumentRepository.save(addData);
    }

    public ListResult<BoardDocumentItem> getDocuments(int pageNum) {
        // 원본 리스트를 가져온다. 근데 우리는 페이징을 해야 하니까 리스트<>가 아니라 페이지<> 형태로 받아온다.
        // 페이징을 해야하기 때문에 몇번째 페이지를 보고 싶은지, 한 페이지 당 몇개의 아이템을 보여줄 건지 알려줘야 한다.
        // 그래서 리스트컨버트서비스.겟페이지에이블(원하는 페이지 번호)을 이용해서 페이지리퀘스트 ( 페이지에이블 구현한 놈이 페이지리퀘스트라서 결국 같은 놈이다)을 넘겨준다
        Page<BoardDocument> originList = boardDocumentRepository.findAll(ListConvertService.getPageable(pageNum));

        List<BoardDocumentItem> result = new LinkedList<>(); // 결과 리스트를 담을 빈 리스트 생성
        for (BoardDocument item : originList.getContent()) { // 원본 리스트에서 원본을 차례대로 하나씩 던져주기 시작한다. 오리진리스트는 페이지 객체라서 리스트를 빼오려면 겟컨텐츠라고 정확히 해서 리스트만 빼와야 한다.
            result.add(new BoardDocumentItem.BoardDocumentItemBuilder(item).build()); // 재가공 해서 리절트에 넣어준다.
        }

        // 리스트컨버트서비스.세팅리절트를 이용해서 리스트를 리스트리절트로 바꿔준다.
        // 근데 이번에는 무조건 1페이지 중 1페이지가 아니기 때문에
        // 원본의 총 갯수 (원본 갯수만큼 재가공해서 보여주기 때문에 원본 갯수 = 재가공 갯수 똑같다)
        // 총 몇페이지인지, 현재 몇페이지인지 다 알려주고 리스트를 리스트리절트로 바꿔달라고 요청한다.
        return ListConvertService.settingResult(
                result,
                originList.getTotalElements(),
                originList.getTotalPages(),
                originList.getPageable().getPageNumber()
        );
    }
}
