package com.wlsendia.pagingtestapi.entity;

import com.wlsendia.pagingtestapi.interfaces.CommonModelBuilder;
import com.wlsendia.pagingtestapi.model.BoardCommentCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardComment {

    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게시글 시퀀스")
    @Column(nullable = false)
    private Long boardDocumentId;

    @ApiModelProperty(notes = "작성자")
    @Column(nullable = false, length = 20)
    private String writerName;

    @ApiModelProperty(notes = "내용")
    @Column(nullable = false, columnDefinition = "TEXT")
    private String contents;

    @ApiModelProperty(notes = "등록일")
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    private BoardComment(BoardCommentBuilder builder) {
        this.boardDocumentId = builder.boardDocumentId;
        this.writerName = builder.writerName;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;

    }

    public static class BoardCommentBuilder implements CommonModelBuilder<BoardComment> {

        private final Long boardDocumentId;
        private final String writerName;
        private final String contents;
        private final LocalDateTime dateCreate;

        public BoardCommentBuilder(Long boardDocumentId, BoardCommentCreateRequest createRequest){
            this.boardDocumentId = boardDocumentId;
            this.writerName = createRequest.getWriterName();
            this.contents = createRequest.getComments();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public BoardComment build() {
            return new BoardComment(this);
        }
    }
}