package com.wlsendia.pagingtestapi.model;

import com.wlsendia.pagingtestapi.entity.BoardComment;
import com.wlsendia.pagingtestapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardCommentItem {

    @ApiModelProperty(notes = "댓글 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "작성자")
    private String writerName;

    @ApiModelProperty(notes = "댓글 내용")
    private String contents;

    @ApiModelProperty(notes = "등록일")
    private LocalDateTime dateCreate;

    private BoardCommentItem(BoardCommentItemBuilder builder){
        this.id = builder.id;
        this.writerName = builder.writerName;
        this.contents = builder.contents;
        this.dateCreate = builder.dateCreate;
    }

    public static class BoardCommentItemBuilder implements CommonModelBuilder<BoardCommentItem>{


        private final Long id;

        private final String writerName;

        private final String contents;

        private final LocalDateTime dateCreate;

        public BoardCommentItemBuilder(BoardComment boardComment){
            this.id = boardComment.getId();
            this.writerName = boardComment.getWriterName();
            this.contents = boardComment.getContents();
            this.dateCreate = boardComment.getDateCreate();

        }

        @Override
        public BoardCommentItem build() {
            return new BoardCommentItem(this);
        }
    }



}
