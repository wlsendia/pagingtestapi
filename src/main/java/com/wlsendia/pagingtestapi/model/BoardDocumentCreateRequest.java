package com.wlsendia.pagingtestapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardDocumentCreateRequest {

    @ApiModelProperty(notes = "게시글 제목(2~100)", required = true)
    @NotNull
    @Length(min = 2, max = 100)
    private String title;

    @ApiModelProperty(notes = "게시글 내용(4~)", required = true)
    @NotNull
    @Length(min = 4)
    private String contests;

    @ApiModelProperty(notes = "작성자 이름", required = true)
    @NotNull
    @Length(max = 20)
    private String writerName;
}
