package com.wlsendia.pagingtestapi.repository;

import com.wlsendia.pagingtestapi.entity.BoardDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardDocumentRepository extends JpaRepository<BoardDocument, Long> {

}
