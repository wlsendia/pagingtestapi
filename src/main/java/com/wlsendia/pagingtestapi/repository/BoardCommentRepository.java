package com.wlsendia.pagingtestapi.repository;

import com.wlsendia.pagingtestapi.entity.BoardComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardCommentRepository extends JpaRepository<BoardComment, Long> {
    Page<BoardComment> findAllByBoardDocumentIdOrderByIdDesc(long boardDocumentId, Pageable pageable);
}