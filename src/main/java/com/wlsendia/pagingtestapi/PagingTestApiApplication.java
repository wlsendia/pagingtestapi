package com.wlsendia.pagingtestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagingTestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PagingTestApiApplication.class, args);
    }

}
